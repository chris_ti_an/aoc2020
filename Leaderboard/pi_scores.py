import operator
from datetime import datetime
import glob
import json
from dateutil import tz

from_zone = tz.tzutc()
to_zone = tz.tzlocal()

def create_localtime_string(timestamp):
    utc_time = datetime.utcfromtimestamp(timestamp);
    utc_time = utc_time.replace(tzinfo=from_zone)
    local_time = utc_time.astimezone(to_zone)
    return local_time.strftime('%Y-%m-%d %H:%M:%S')

start_times = {}
for file in glob.glob('member*.json'):
    member_data = json.load(open(file))
    start_times[member_data['name']] = member_data['start_times']

events = {}
def add_event (timestr, data):
    if not timestr in events:
        events[timestr] = []
    events[timestr].append(data)

mintime = 100000
mintimesolver = (1,1,'')

scores = json.load(open("454363.json"))
members = scores['members']
nr_members = len(members)
scores = {}
solving_times = {}
for d in range (1,26):
    solving_times [d] = {1: {}, 2:{}}
for m in members:
    data = members[m]
    name = data['name']
    scores[name] = 0
    completions = data['completion_day_level']
    for day in completions:
        timestr = start_times[name][day]
        time = datetime.strptime (timestr, '%Y-%m-%d %H:%M:%S')
        add_event(timestr, (name, int(day), 0, time-time))
        parts = completions[day]
        for p in sorted(parts):
            t0 = time
            timestr = create_localtime_string(int(parts[p]['get_star_ts']))
            time = datetime.strptime(timestr, '%Y-%m-%d %H:%M:%S')
            dt = time - t0
            if (dt.seconds<mintime):
                mintime = dt.seconds
                mintimesolver = (int(day), int(p), name)
            add_event(timestr, (name, int(day), int(p), dt))
            solving_times[int(day)][int(p)][name] = dt.seconds

print()
# print log
for e in sorted(events.keys()):
    for entry in events[e]:
        name, day, part, dt = entry
        if part == 0:
            print("{0}: {1} starts day {2}"
                  .format(e, name, day))
        else:
            print("{0}: {1} solves day {2}, part {3} in {4} seconds"
                  .format(e, name, day, part, dt.seconds))

print()
for day in solving_times:
    for part in solving_times[day]:
        times = solving_times[day][part]
        if len(times)>0:
            print("Day {0} part {1}".format(day, part))
            score = nr_members
            sorted_times = sorted(times.items(), key=operator.itemgetter(1))
            for (name, dt) in sorted_times:
                scores[name] += score
                print ('    {0}: {1} seconds scores {2} points (now {3})'.format(name, dt, score, scores[name]))
                score -= 1

print()
print ('fastest solution: {0} day {1} part {2} in {3} seconds'
       .format(mintimesolver[2], mintimesolver[0], mintimesolver[1], mintime))
print()
rank = sorted(scores.items(), key=operator.itemgetter(1), reverse=True)
for (name, scr) in rank:
    print ("{0} - {1} points".format(name, scr))