# -*- coding: utf-8 -*-


import re

text = """BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL"""

text = open("input.txt").read()

lines = text.split()

def decode(s):
    #trow = s[:7].replace('B', '1').replace('F', '0')
    #tseat = s[7:].replace('R', '1').replace('L', '0')
    id = s.replace('B', '1').replace('F', '0').replace('R', '1').replace('L', '0')
    row = int(s[:7].replace('B', '1').replace('F', '0'), 2)
    #print (s, trow, tseat, int(trow,2), int(tseat,2), int(id,2))
    return int(id,2), row

seats = []
for l in lines:
    id, row = decode (l)
    if (row>0 and row<127):
        seats.append(id)
seats.sort()
for i in range(len(seats)-1):
    if (seats[i+1] - seats[i]) == 2:
        print (seats[i]+1)

