# -*- coding: utf-8 -*-

import copy
import re

invalid_text = """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"""

valid_texts="""pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"""

text = open("input.txt").read()



lines = text.split('\n')

def is_valid(p):
    hasallkeys = 'byr' in p.keys() and 'iyr' in p.keys() and 'eyr' in p.keys() and 'hgt' in p.keys() and 'hcl' in p.keys() and 'ecl' in p.keys() and 'pid' in p.keys()
    if not hasallkeys:
        return False
    byr = int(p['byr'])
    if byr < 1920 or byr > 2002:
        print (p['byr'] + ' invalid byr')
        return False
    iyr = int(p['iyr'])
    if iyr < 2010 or iyr > 2020:
        print (p['iyr'] + ' invalid iyr')
        return False
    eyr = int(p['eyr'])
    if eyr < 2020 or eyr > 2030:
        print (p['eyr'] + ' invalid eyr')
        return False
    if p['hgt'][-2:] == 'cm':
        hgtcm = int(p['hgt'][:-2])
        if hgtcm < 150 or hgtcm > 193:
            print (p['hgt'] + ' invalid hgt')
            return False
    elif p['hgt'][-2:] == 'in':
        hgtin = int(p['hgt'][:-2])
        if hgtin < 59 or hgtin > 76:
            print (p['hgt'] + ' invalid hgt')
            return False
    else:
        print(p['hgt'] + ' invalid hgt')
        return False
    if not re.fullmatch('#[a-f0-9]{6}', p['hcl']):
        print(p['hcl'] + ' invalid hcl')
        return False
    if p['ecl'] not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
        print(p['hcl'] + ' invalid hcl')
        return False
    if not re.fullmatch('\d{9}', p['pid']):
        print(p['pid'] + ' invalid pid')
        return False

    return True



passports = []
passport = {}
for l in lines:
    if l=='':
        #print (passport)
        passports.append(passport)
        passport = {}
    else:
        fields = l.split()
        for f in fields:
            items = f.split(':')
            passport[items[0]] = items[1]
passports.append(passport)
nrValid = 0
for p in passports:
    if is_valid(p):
        nrValid += 1


print(nrValid)

